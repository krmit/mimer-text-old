// Very inspired by https://stackoverflow.com/a/33206814

export const SeqLink = (link: string, msg: string) =>
  LINK + link + BELL + msg + LINK + BELL;
export const Seq = (...list: (string | number)[]) =>
  START + list.join(SEPARATOR) + END;

export const ESC = "\x1b";
export const BELL = ESC + "\\";
export const LINK = ESC + "]8;;";
export const START = ESC + "[";
export const END = "m";
export const CLEAR = "0";
export const SEPARATOR = ";";
export const RESET = START + CLEAR + END;
export const RGB = "38;2";
export const RGB_BG = "48;2;";

export const BLACK = "30";
export const RED = "31";
export const GREEN = "32";
export const YELLOW = "33";
export const BLUE = "34";
export const BROWN = Seq(RGB, 0xa5, 0x2a, 0x2a);
export const MAGENTA = "35";
export const CYAN = "36";
export const PURPLE = Seq(RGB, 0x80, 0x00, 0x80);
export const PINK = Seq(RGB, 0xff, 0xc0, 0xcb);
export const ORANGE = Seq(RGB, 0xff, 0xa5, 0x00);
export const GRAY = Seq(RGB, 0x80, 0x80, 0x80);
export const WHITE = "37";

export const COLOR = {
  black: BLACK,
  red: RED,
  green: GREEN,
  yellow: YELLOW,
  blue: BLUE,
  brown: BROWN,
  magenta: MAGENTA,
  cyan: CYAN,
  purple: PURPLE,
  pink: PINK,
  orange: ORANGE,
  gray: GRAY,
  white: WHITE,
};

export const BLACK_BG = 40;
export const RED_BG = 41;
export const GREEN_BG = 42;
export const YELLOW_BG = 43;
export const BLUE_BG = 44;
export const BROWN_BG = Seq(RGB_BG, 0xa5, 0x2a, 0x2a);
export const MAGENTA_BG = 45;
export const CYAN_BG = 46;
export const PURPLE_BG = Seq(RGB_BG, 0x80, 0x00, 0x80);
export const PINK_BG = Seq(RGB_BG, 0xff, 0xc0, 0xcb);
export const ORANGE_BG = Seq(RGB_BG, 0xff, 0xa5, 0x00);
export const GRAY_BG = Seq(RGB_BG, 0x80, 0x80, 0x80);
export const WHITE_BG = 47;

export const COLOR_BG = {
  black: BLACK_BG,
  red: RED_BG,
  green: GREEN_BG,
  yellow: YELLOW_BG,
  blue: BLUE_BG,
  brown: BROWN_BG,
  magenta: MAGENTA_BG,
  cyan: CYAN_BG,
  purple: PURPLE_BG,
  pink: PINK_BG,
  orange: ORANGE_BG,
  gray: GRAY_BG,
  white: WHITE_BG,
};

export const UNDERLINE = 4;
export const OVERLINE = 53;
export const LINETHROUGH = 9;
export const BOLD = 1;

export const STYLE = {
  underline: UNDERLINE,
  overline: OVERLINE,
  strikethrough: LINETHROUGH,
  bold: BOLD,
};

export const DE_CODE = {
  [BLACK]: "BLACK",
  [RED]: "RED",
  [GREEN]: "GREEN",
  [YELLOW]: "YELLOW",
  [BLUE]: "BLUE",
  [BROWN]: "BROWN",
  [MAGENTA]: "MAGENTA",
  [CYAN]: "CYAN",
  [PURPLE]: "PURPLE",
  [PINK]: "PINK",
  [ORANGE]: "ORANGE",
  [GRAY]: "GRAY",
  [WHITE]: "WHITE",
  [BLACK_BG]: "BLACK_BG",
  [RED_BG]: "RED_BG",
  [GREEN_BG]: "GREEN_BG",
  [YELLOW_BG]: "YELLOW_BG",
  [BLUE_BG]: "BLUE_BG",
  [BROWN_BG]: "BROWN_BG",
  [MAGENTA_BG]: "MAGENTA_BG",
  [CYAN_BG]: "CYAN_BG",
  [PURPLE_BG]: "PURPLE_BG",
  [PINK_BG]: "PINK_BG",
  [ORANGE_BG]: "ORANGE_BG",
  [GRAY_BG]: "GRAY_BG",
  [WHITE_BG]: "WHITE_BG",
  [SEPARATOR]: "SEPARATOR",
  [CLEAR]: "CLEAR",
  [UNDERLINE]: "UNDERLINE",
  [OVERLINE]: "OVERLINE",
  [LINETHROUGH]: "LINETHROUGH",
  [BOLD]: "BOLD",
};

export const DeSeq = (msg: string, shorthand = true) => {
  let result = "";
  let status: "" | "first" | "start" | "first-start" | "seq" = "first";
  let item = "";
  for (const letter of msg) {
    switch (status) {
      case "start":
        if (letter === "[") {
          result += '"+START';
          status = "seq";
        } else {
          result += "ESC";
        }
        break;
      case "first-start":
        if (letter === "[") {
          result += "START";
          status = "seq";
        } else {
          result += "ESC";
        }
        break;
      case "seq":
        if (letter === ";" || letter === "m") {
          result += "+";
          result += DE_CODE[item];
          if (letter === ";") {
            result += "+;";
            item = "";
          } else {
            result += '+END+"';
            item = "";
            status = "";
          }
        } else {
          item += letter;
        }
        break;
      case "first":
        if (letter === ESC) {
          status = "first-start";
        } else {
          result += '"' + letter;
        }
        break;
      default:
        if (letter === ESC) {
          status = "start";
        } else {
          result += letter;
        }
        break;
    }
  }

  if (result.slice(-2) === '+"') {
    result = result.slice(0, -2);
  }

  if (shorthand) {
    result = result.replace("START+CLEAR+END", "RESET");
  }
  return result;
};
