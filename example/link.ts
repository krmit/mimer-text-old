"use strict";
import {
  t,
  Text,
  TextToStringSync,
  TextToTerminalSync,
  TextToJsonSync,
} from "../src";
import { testWith } from "./tools";

let result = t("Mimer IT").link("https://htsit.se");
console.log(TextToTerminalSync(result));

let result2 = t("Mimer IT").red.bold.link("https://htsit.se");
console.log(TextToTerminalSync(result2));

let result3 = t("Mimer", t(" IT")).red.bold.link("https://htsit.se");
console.log(TextToTerminalSync(result3));

let nested = t(
  t("green ").green,
  t("red ", t("blue ").blue, t("Mera "), t("Test").lineThrough).red.underline,
  " !"
).link("https://htsit.se");
console.log(TextToTerminalSync(nested));
