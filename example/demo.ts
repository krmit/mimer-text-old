"use strict";
import { t } from "../src/";
import jsome from "jsome";
import { testWith } from "./tools";

let hello = t("Hej").red;
let helloAndBy = t("Hej", " ", t("Hej då").bold).red;
let headline = t("Hello").headline;
let twoText = t(t("Test ").green, "and more test").underline;
let nested = t(
  t("green ").green,
  t("red ", t("blue ").blue, t("Mera "), t("Test").lineThrough).red.underline,
  "!"
).bold;

//testWith("Hello", hello);
//testWith("Hello And By", helloAndBy);
//testWith("Headline", headline);
testWith("Nested", nested);

//testWith("Two texr", twoText);
