"use strict";
import { t, Text, TextToTerminal, TextToWeb, TextToWebSync } from "../src";

let hello = t(
  "Hej ",
  t("hej ").source("./test/Assets/hello.text.json")
).underline;
let hello2 = t(
  "Hej ",
  t("hej ").source("./test/Assets/more-source.text.json"),
  " hallå"
).underline;
let hello3 = t("Hej ").blue.underline;
//let helloAndBy = t("Hej", " ", t("Hej då").bold).red.addClass("news");
//let headline = t(t("Hello").headline.addClass("news"), t("Test")).block;
//let list = t(t("green", "???", "!!!").green, t("Good ", " !")).list;
//let list2 = t(t("green", "???", "!!!").green, t("Good ", t(" !").blue).red, "II").list;

let result = TextToTerminal(hello2);
result.then(console.log);

let result2 = TextToWebSync(hello3);
console.log(result2);
result = TextToWeb(hello);
result.then(console.log);
