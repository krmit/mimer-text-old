"use strict";
import { t } from "../src";
import jsome from "jsome";
import { testWith } from "./tools";

let box = t("This is a test!").border;

testWith("A simpel box", box);

let box_more_lines = t("This is anouther test!\nWith more lines").border;

testWith("A multi line in box", box_more_lines);

let box_colors = t(
  "This is anouther ",
  t("test!").cyan.underline,
  "\nWith ",
  t("colors ").green,
  "lines"
).border;

testWith("Colors in box", box_colors);
