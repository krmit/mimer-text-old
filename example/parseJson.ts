"use strict";
import { t, JsonToTextSync } from "../src";
import { testWith } from "./tools";

function printEscape(text: string) {
  return text.replace(/[\u001b\u009b]/g, "\\\u001b\\u009b");
}

let text_json: any = {
  fontWeight: "bold",
  data: [
    {
      color: "green",
      data: ["green "],
    },
    {
      color: "red",
      textDecoration: "underline",
      data: [
        "red ",
        {
          color: "blue",
          data: ["blue "],
        },
        "Mera ",
        {
          textDecoration: "line-through",
          data: ["Test"],
        },
      ],
    },
    " !",
  ],
};

let text = JsonToTextSync(text_json);

testWith("A not so simpel text", text);

let text_with_error_json: any = {
  textDecoration: "line-around",
  data: "Test",
};

try {
  let text_with_error = JsonToTextSync(text_with_error_json);
} catch (err) {
  console.log(err);
}
