"use strict";
import {
  t,
  Text,
  TextToStringSync,
  TextToTerminalSync,
  TextToJsonSync,
} from "../src/";
import { testWith } from "./tools";

// Problem with newline, should not be inherit.
let test1 = t(10).fixed(6);
let test2 = t(12.34).fixed(3);
let test3 = t(12.3456789).fixed(3);
let test4 = t(12.3456789).fixed(0);
let test5 = t(12.3456789).fixed(5);

testWith("10", test1);
testWith("12.34", test2);
testWith("12.345", test3);
testWith("12", test4);
testWith("12.34567", test5);
