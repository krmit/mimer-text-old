"use strict";
import { t, Text, TextToTerminal } from "../src";
import * as readline from "readline";

let box = t("This is a test!").border;

TextToTerminal(box);

async function main() {
  let prompt = t("Detta är en prompt> ").bold.blue.prompt;

  let a = await TextToTerminal(prompt);
  console.log(a);
}

main();
