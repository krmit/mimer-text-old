"use strict";
import { t } from "../src/";
import { testWith } from "./tools";

// Problem with newline, should not be inherit.
let test1 = t(0.25).percent;
let test2 = t(0.75).percent;
let test3 = t(0.1).percent;
let test4 = t(1.25).percent;
let test5 = t(0.125).percent;

testWith("25%", test1);
testWith("75%", test2);
testWith("10%", test3);
testWith("125%", test4);
testWith("12.5%", test5);
