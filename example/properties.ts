"use strict";
import { t, TextToJsonSync } from "../src";
import { testWith } from "./tools";

let hello = t("Hej!").addClass("hello");

console.log(TextToJsonSync(hello));

/*
let helloAndBy = t("Hej", " ", t("Hej då").bold).red;
let headline = t("Hello").headline;
let nested = t(
  t("green ").green,
  t("red ", t("blue ").blue, t("Mera "), t("Test").lineThrough).red.underline,
  " !"
).bold;
*/
