import {
  t,
  Text,
  TextToStringSync,
  TextToTerminalSync,
  TextToJsonSync,
} from "../src/";
import { DeSeq } from "../terminal-sequences/main";

export function testWith(msg: string, text: Text) {
  console.log("=========== " + msg + " ============");
  // console.log(TextToString(text));
  // jsome(TextToJson(text));
  const terminal_result = TextToTerminalSync(text);
  console.log(terminal_result);
  console.log(DeSeq(terminal_result));
}
