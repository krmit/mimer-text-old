"use strict";
import { t, Text, TextToWebSync } from "../src";
import jsome from "jsome";

let hello = t("Hej").red;
let helloAndBy = t("Hej", " ", t("Hej då").bold).red.addClass("news");
let code = t("const a = 0;\na++;").code("js");
let math = t("sum_(i=1)^n i^3=((n(n+1))/2)^2").math;
let headline = t(t("Hello").headline.addClass("news"), t("Test")).block;
let nested = t(
  t("green ").green,
  t("red ", t("blue ").blue, t("Mera "), t("Test").lineThrough).red.underline,
  " !"
).bold.block;
let list = t(t("green", "???", "!!!").green, t("Good ", " !")).list;
let list2 = t(
  t("green", "???", "!!!").green,
  t("Good ", t(" !").blue).red,
  "II"
).list;
let list3 = t(
  t("I ").green,
  t("II ", "hej").red,
  "III",
  "IV",
  "V",
  "VI",
  "VII",
  "VIII",
  "IX",
  "X",
  "XI",
  "XII",
  t("XIII").bold.blue,
  "XIV"
).orderedList;

let result = TextToWebSync(code);
//let result = TextToWeb(t("Mimer IT").image("https://htsit.se"));
console.log(result);
//console.log(TextToWeb( helloAndBy));
//console.log(TextToWeb( headline));
//console.log(TextToWeb( nested));
