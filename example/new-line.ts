"use strict";
import { t } from "../src/";
import { testWith } from "./tools";

// Problem with newline, should not be inherit.
let hello = t("Hello", " ", "world!").newline;
testWith("Hello", hello);

testWith("Inner newline", t(t("Hello!").newline, t("By!")));
