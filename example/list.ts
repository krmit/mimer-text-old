"use strict";
import { t } from "../src/";
import { testWith } from "./tools";
/*
let list = t(t("green ").green, t("red ").red, " !").list;

testWith("A simpel list", list);

let uolist = t(t("A ").green, t("B ").red, "C").unorderedList;

testWith("A simpel list", uolist);

let olist = t(t("A ").green, t("B ").red, "C").orderedList;

testWith("A simpel list", olist);
*/
let rlist = t(
  t("I ").green,
  t("II ").red,
  "III",
  "IV",
  "V",
  "VI",
  "VII",
  "VIII",
  "IX",
  "X",
  "XI",
  "XII",
  t("XIII").bold.blue,
  "XIV"
).orderedList;

testWith("A simpel list", rlist);
