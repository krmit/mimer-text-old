"use strict";
import { t } from "../src/";
import { testWith } from "./tools";

// Problem with newline, should not be inherit.
let hello6 = t("Hello", " ", "world!").span(6);
let foo6 = t("Foo").span(6);
let hello10 = t("Hello", " ", "world!").span(10);

testWith("Hello size 6", hello6);
testWith("Foo size 6", foo6);
testWith("Hello size 10", hello10);
