  * !Inför geters. Ta bort getters?
  * !Gör en funkton för att testa att output är rätt oavsett vilketn
  platform som används.
  * !Gör en parse tester som testar att ingen information försviner, kan vara svårt ...
  * !Strukturera upp testerna så de blir enkla.
    * !En bas test för varje output type.
    * !Dela upp testerna i olika anvädningsområden.
  * !Add property class.

## Importent 1h

 * !Fundera ignneom uppdelning mellan interative och den vanliga versionen.
 * !Implmentera funktioner för att underlätta detta.

## More styles 1h

  * !Image support. Så enkelt som möjligt.
  * !Link support.  

## HTML 3h

  * !Fundera igenom designen, antagligen span + css.
  * !Implmentera enkel design.
  * !Implmentera bas tester. 
  * !Fixa också klasser.
  * !Gör så tagar översätt till olika element.
  * !Fixa bilder och länkar.
  * !Fixa listor
  * !Implmentera text för olika anvädninsområden.

## Layoute, bara för html! 2h

  * !Inspiredad av block.
  * !Gör så att i webb så blir nästa medelande en paragraf.

## Source

 * !A text will be replace of result of link.

## More types

  * !Code(lang) - för att visa kod.
  * !Math - För att visa matematik.
 
## End for 0.5

 * !fmt koden och titta igenom den.
 * !Publisera nya version av packetet.

## TextTo

 * !Installera versionhanterings program och updatera alla versionerna av packet.
 * !Fundera igenom om nuvarnde uppdelning är bra eller om det tre typern av funktion kan slås ihop till en. *Tror det är bäst med två och försöka ha så mycket kod gemnsamt som möjligti TextTo*.
 * !Gå ignom och fundera på en bättre design.
 * !Gör ny debug funktion som gör det enkel att läsa av vad som händer.
 * !Updated demos.
 * !Gör om till två funktioner.
 * !Gör så den funktion som är rekusiv är inte den första som anropas, utan NodeTo.
 * !Se till att vi får två nya callabcks funktoner som körs en gång när funktionen anropas och avslutas. Till skillnad från start och slut vid varje tag, begin and finish.
 * !Inför två  konfigueringsparameter. Link och Level.
 * !Båda dessa har en templet som sätts av funktionen.
 * !Ha en configur som ges till TextTo funktionen.
 * Läs igenom koden TextTo terminal en gång till.
 * Gå igenom viktiga demos of fixa problem för terminal.
 * Gå igenom viktiga demos of fixa problem för string.
 * Gå igenom viktiga demos of fixa problem för web.

# Testa 

 * Gå ingom testerna få bort kompiler erraror och få dem använda termminal-sequence.
 * Se till att testeran återigen börjar fungera

# Avsluta
 * Gå igenom och gör allt som kan vara gemensmat till gemensma kod i TextTo.
   - Source
   - Gör så att strängar och tomma text inte skapar nya noder. 
 * Se till att varje funktion är tre eller två delade. if-case-create. 
 * Börja kommentera koden.

 ## Tema

 * Skilj på  två de olia funktiner. Style och tema.
 * Implmentera så teman går att sätta.
 * Gör tema funcktion som sätter tema för olika element baserat på json.
 * Gör så teman kan sättas för ett helt backend.
 * Gör ett inbyggt default.
 * Gör så detta kan konfigueras med josn filer.

## HTML huvud
  * Gör så att webben kan konfiguerars till att ha en header och ett avslut.
  * Gör så detta huvud kan configueras.
  * Gör så att AsciiMath Importeras.
  * Gör så något bibliotek för higlight importeras.

## Bugar
  * Fixa bugen med code.
  * Gör så Text får sin egen fil.

## Version 0.6

 ## Prompt

  * propmt egenskap som ger namn till input värden.
  * Skapa test för prompt.
  * Gör olika input värden såsom text, heltal och decimaltal.
  * Gör en selektor så att värden kan väljas från en meny.
  * Gör radion button.

## Server



## Client



## Multi-server



## Version 0.7

## Gör så terminal-sequences får eget packet

## Överför test till en egen fil.
* Ska test vara en funktion i mimer-text?

## Gå igenom alla demon och skriv fler test
  * Organisera upp demona bättre.

## Nya funktioner

  * Fixa till list för terminal och web. Vi tycks använda tre olika former av listor var av en inte funkar i web, design bättre. 
  * Gör så elementerna efter en headline blir en paragraf.
  * Få bort onödig html style element. Gör ett objet och kolla vilka style som redan är satta.
  * Skriv om terminal så den inte använnder onödiga escapcarateärer, borde också lösa andra problem med arv av egenskaper.
  * Gör så att det inte behövs anges vilket språk som coden kommer ifrån i code.

## Code och math
  * Inför en kort version av code, en version som används som span.
  * Inför en kort version av math, en version som används som span.
  * Inför en enkel terminal version av code.
  * Inför en enkel terminal version av math.

## Enekl UI design
*Mest för att se vad som kan göras i framtiden.*

* Fundera igenom UI
* Hidden - Hur blädra igenom kod.
* Views - en klient kan ha många viws
* Catchning
* Inte ställa för mågna frågor till servern.

## Bugar

## Version 0.8

* Egenskaper ärvs inte av source för terminalen.

# Test

  * Gå igenom text och förbättra språket.
  * Gör tester för async eller anpassa till nya design.

## Dokumentation
* Men inte allt för mycket*

  * Börjar Kommentera koden.
  * Gå igenom all kod och skriv TypeDoc för den.
  * Skriv en bra README
  * Läs igenom alla tester och skriv bättre dokumentation.

## Arkiv

* Nollställ arkivet.
* Skriv regler för hur commit medelanden ska skrivas.
* Överför alla att göra och funktions erbjudande efter version 0.8 till issue.
* Gå igenom alla packet och updatera till senaste versionen.

## Bugar

## Publisera version 0.9

* Skapa ett packet på npm.

## Bugar

* Indent skapas ibland för många gånger.
* indent not working with pure strings in web.
* Fixa fixed så att den fungerar.
* Fixa newline så den alltid fungerar.
* Underline/Linetrought är inte oberonde av varandra och fungera inte tillsammans. Se demo nested.


