# Mimer text

A way to give style and more to small texts.

**Not completed yet**

## Install


```
$ npm install @mimer/text
```

## Usage


```
let helloAndBy = t("Hello", " ", t("By", "!").bold).red;

console.log(TextToTerminal(text));
```

### Environment

It exsists two environment *client* or *local*.

### Backends

All backens exist in an normal version that is asyncron. And in a sycron version with limit feuthures to be used it the norml version is not practical to use.

### 

## API

### Tools
- add *string*
- class *string*
- theme *path|theme config*
- addTag *tag*
- hasTag *tag*
- addClass *string*
- hasClass *string*

### Theme with Tags

- titel
- headline
- subheadline *1-6*
- selection
- code *lang*
- block
- box
- important
- fixed
- list
- orderedList
- unorderedList
- border
- percent

### Style
- span
- newline

#### Color
- red
- blue
- green
- yellow
- magenta
- cyan
- gray

#### Font
- bold
- underline
- lineThrough
- overline
- 

### UI
- prompt
- link
- image
- code
- source
- math

## Contribute

Please feel free to open an issue or submit a pull request.

## Changelog

 * **0.5.0** *2022-02-13* Implemented web backend. Added image, source, link and more. Started on redesign of common code.
 * **0.4.0** *2021-02-23* Implemented a simpleversion box and interactive functions for terminal. Also parsing of data from Json.
 * **0.3.0** *2020-03-15* Published first version.


## License

ICS, see LICENSE file


## Author

**© 2019-2021 [Magnus Kronnäs](https://magnus.kronnas.se)**
