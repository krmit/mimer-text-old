import { testText } from "../src/testFunctions";
import { t } from "../src/index";

describe("Headline", () => {
  describe("Test of a simpel headline", () => {
    it("A singel headline", () => {
      const result = t("This is a headlien!").headline;
      testText(result, {
        string: "This is a headlien!",
        web: '<h1 class="headline">This is a headlien!</h1>\n',
      });
    });
    it("A headline with style", () => {
      const result = t(t("This is a ").bold, t("block!").green).headline;
      testText(result, {
        string: "This is a block!",
        /* space between span:s is a bug */
        web: `<h1 class="headline">    <p style="font-weight:bold">This is a </p>
    <p style="color:green">block!</p>
</h1>\n`,
      });
    });
    it("A headline followed by a paragraph", () => {
      const result = t(
        t("This is a headlien!").headline,
        t("A text").addClass("description")
      ).block;
      /* Json representation do not work without remove class */
      testText(result, {
        string: "This is a headlien!A text",
        /* Extra newline is a bug */

        web: `<div>
    <h1 class="headline">This is a headlien!</h1>
    <p class="description">A text</p>

</div>`,
      });
    });
  });
});
