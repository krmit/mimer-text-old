import { testText } from "../src/testFunctions";
import { t, Text, TextToJson } from "../src/index";
import colors from "colors/safe";

const ESC = "\x1b";
const BELL = ESC + "\\";
const LINK = ESC + "]8;;";

describe("Text has a link", () => {
  describe("Simple Link tests", () => {
    it("Text is linked", () => {
      const result = t("Hello World!").link("http://exampel.com");
      testText(result, {
        string: "Hello World!",
        terminal:
          LINK + "http://exampel.com" + BELL + "Hello World!" + LINK + BELL,
        web: '<a href="http://exampel.com"><span>Hello World!</span></a>',
      });
    });
    it("Text with a blue word", () => {
      const result = t("Hello ", t("World").green, "!").link(
        "http://exampel.com"
      );
      testText(result, {
        string: "Hello World!",
        terminal:
          LINK +
          "http://exampel.com" +
          BELL +
          "Hello " +
          colors.green("World") +
          "!" +
          LINK +
          BELL,
        web: '<a href="http://exampel.com"><span>Hello <span style="color:green">World</span>!</span></a>',
      });
    });
  });
});
