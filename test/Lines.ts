import { testText } from "../src/testFunctions";
import { t, Text, TextToJson } from "../src/index";

describe("toJSON with Text result in strings", () => {
  let hello: Text;

  beforeEach(function () {
    hello = new Text(["Hello", " ", "World", "!"]);
  });

  describe("Simple test of toJSON", () => {
    it("Add a return to end of line", () => {
      const result = hello.newline;
      testText(result, {
        string: "Hello World!\n",
        web: "<span>Hello World!</span><br />",
      });
    });
    it("Add a new line", () => {
      const result = t(hello.newline, t("Good bye ", "World", "?"));
      testText(result, {
        string: "Hello World!\nGood bye World?",
        web: "<span><span>Hello World!</span><br /><span>Good bye World?</span></span>",
      });
    });
    it("Add many lines", () => {
      const result = t(
        hello,
        t("It is ", "the ", "final countdown", "!").newline,
        t("5", "!!!!!").newline,
        t("4", "!!!!").newline,
        t("3", "!!!").newline,
        t("2", "!!").newline,
        t("1", "!").newline,
        t("0").newline,
        t("Good ", "New ", "Year", "!")
      );
      testText(result, {
        string:
          "Hello World!It is the final countdown!\n5!!!!!\n4!!!!\n3!!!\n2!!\n1!\n0\nGood New Year!",
        web: "<span><span>Hello World!</span><span>It is the final countdown!</span><br /><span>5!!!!!</span><br /><span>4!!!!</span><br /><span>3!!!</span><br /><span>2!!</span><br /><span>1!</span><br /><span>0</span><br /><span>Good New Year!</span></span>",
      });
    });
  });
});
