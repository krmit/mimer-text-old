import { testText } from "../src/testFunctions";
import { t } from "../src/index";

describe("Math", () => {
  describe("Test of simpel math.", () => {
    it("Standar test math equation", () => {
      const result = t("sum_(i=1)^n i^3=((n(n+1))/2)^2").math;
      testText(result, {
        string: "sum_(i=1)^n i^3=((n(n+1))/2)^2",
        web: `\`
sum_(i=1)^n i^3=((n(n+1))/2)^2
\`
`,
      });
    });
  });
});
