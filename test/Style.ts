import { testText } from "../src/testFunctions";
import { t, Text, TextToJson } from "../src/index";
import colors from "colors/safe";

describe("Text with style", () => {
  describe("Simple style tests", () => {
    it("Text with a bold word", () => {
      const result = t("Hello ", t("World").bold, "!");
      testText(result, {
        string: "Hello World!",
        web: '<span>Hello <span style="font-weight:bold">World</span>!</span>',
      });
    });
  });
  describe("Mixed style tests", () => {
    it("Text with a blue word and bold word", () => {
      const result = t(t("Hello").bold, " ", t("World").blue, "!");
      testText(result, {
        string: "Hello World!",
        terminal: colors.bold("Hello") + " " + colors.blue("World") + "!",
        web: '<span><span style="font-weight:bold">Hello</span> <span style="color:blue">World</span>!</span>',
      });
    });
    it("Text with a red and bold word", () => {
      const result = t("Hello ", t("World").red.bold, "!");
      testText(result, {
        string: "Hello World!",
        terminal: "Hello " + colors.bold(colors.red("World")) + "!",
        web: '<span>Hello <span style="color:red; font-weight:bold">World</span>!</span>',
      });
    });
  });
});
