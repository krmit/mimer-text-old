import { assert } from "chai";
import colors from "colors/safe";
import { t, Text, JsonToText, TextToTerminalSync } from "../src/index";

describe("Parse JSON to a Text object", () => {
  describe("Parse a Json and print result to terminal", () => {
    it("Default box around a text with some colors", () => {
      const text = JsonToText({
        borderStyle: "solid",
        data: [
          "This is anouther ",
          {
            color: "cyan",
            textDecoration: "underline",
            data: ["test!"],
          },
          "\nWith ",
          {
            color: "green",
            data: ["colors "],
          },
          "lines",
        ],
      });
      const result = TextToTerminalSync(text);
      assert.equal(
        "\n" + result,
        `
 ┌──────────────────────┐
 │This is anouther \u001b[4m\u001b[36mtest!\u001b[39m\u001b[24m│ 
 │With \u001b[32mcolors \u001b[39mlines     │ 
 └──────────────────────┘
`
      );
    });
  });
  describe("Errors from bad Json data", () => {
    it("Default box around a text with some colors", () => {
      try {
        const text = JsonToText({
          textDecoration: "line-around",
          data: ["Test"],
        });
      } catch (err) {
        assert.equal(err, `line-around is not a text decoration.`);
      }
    });
  });
});
