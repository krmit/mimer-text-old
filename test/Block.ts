import { testText } from "../src/testFunctions";
import { t } from "../src/index";

describe("Block", () => {
  describe("Test of definition of block", () => {
    it("A simpel image representation, it dose nothing", () => {
      const result = t("This is a block!").block;
      testText(result, {
        string: "This is a block!",
        web: "<div>\nThis is a block!\n</div>",
      });
    });
    it("A simpel image representation, it dose nothing", () => {
      const result = t(t("This is a ").bold, t("block!").green).block;
      testText(result, {
        string: "This is a block!",
        /* space between span:s is a bug */
        web: `<div>
    <span style="font-weight:bold">This is a </span>    <span style="color:green">block!</span>
</div>`,
      });
    });
  });
});
