import { assert } from "chai";
import { t, Text, TextToJson } from "../src/index";

describe("toJSON with Text result in strings", () => {
  let hello: Text;

  beforeEach(function () {
    hello = new Text(["Hello", "World", "!"]);
  });

  describe("Simple test of toJSON", () => {
    it('Simpel JSON representation of the text "Hello World"', () => {
      const result = TextToJson(hello);
      assert.deepEqual(result, { data: ["Hello", "World", "!"] });
    });
    it("Apend a new text", () => {
      const result = TextToJson(hello.add(t("?", "!")));
      assert.deepEqual(result, {
        data: ["Hello", "World", "!", { data: ["?", "!"] }],
      });
    });
  });
});
