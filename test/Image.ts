import { testText } from "../src/testFunctions";
import { t } from "../src/index";

describe("Image", () => {
  describe("Test of url to image", () => {
    it("A simpel image representation, it dose nothing", () => {
      const result = t("This is a image!").image("./data/test.png");
      testText(result, {
        string: "This is a image!",
        web: '<img src="./data/test.png" alt="This is a image!" />',
      });
    });
  });
});
