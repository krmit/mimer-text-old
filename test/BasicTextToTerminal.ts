import { assert } from "chai";
import { t, Text, TextToTerminalSync } from "../src/index";

describe("Terminal output for Text result in strings", () => {
  let hello: Text;

  beforeEach(function () {
    hello = new Text(["Hello", " ", "World", "!"]);
  });

  describe("Simple test of TextToTerminal", () => {
    it('Simpel JSON representation of the text "Hello World"', () => {
      const result = TextToTerminalSync(hello);
      assert.deepEqual(result, "Hello World!");
    });
    it("Apend a new text", () => {
      const result = TextToTerminalSync(hello.add(t("?", "!")));
      assert.deepEqual(result, "Hello World!?!");
    });
  });
});
