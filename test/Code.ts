import { testText } from "../src/testFunctions";
import { t } from "../src/index";

describe("Code", () => {
  describe("Test of simpel js code.", () => {
    it("Test of short code", () => {
      const result = t("const a = 0;\na++;").code("js");
      testText(result, {
        string: "const a = 0;\na++;",
        web: `<pre>
<code class="js-lang">
const a = 0;
a++;
</code>
</pre>
`,
      });
    });
  });
});
