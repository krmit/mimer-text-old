import { t, Text } from "../src/index";
import { testText } from "../src/testFunctions";

describe("Simple test of testFunction", () => {
  let hello: Text;

  beforeEach(function () {
    hello = new Text(["Hello", " ", "World", "!"]);
  });
  describe("Basic test", () => {
    it("Test JSON representation", () => {
      testText(hello);
    });
    it("Test a string", () => {
      testText(hello, { string: "Hello World!" });
    });
    it("Test a terminal string", () => {
      testText(hello, { terminal: "Hello World!" });
    });
  });
});
