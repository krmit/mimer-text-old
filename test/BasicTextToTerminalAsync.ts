import { assert } from "chai";
import { t, Text, TextToTerminal } from "../src/index";

describe("Terminal output for Text result in a promise of a string", () => {
  let hello: Text;

  beforeEach(function () {
    hello = new Text(["Hello", " ", "World", "!"]);
  });

  describe("Simple test of TextToTerminal", () => {
    it('Simpel JSON representation of the text "Hello World"', (done) => {
      const result = TextToTerminal(hello);
      result.then((test) => {
        assert.deepEqual(test, "Hello World!");
        done();
      });
    });
    it("Apend a new text", (done) => {
      const result = TextToTerminal(hello.add(t("?", "!")));
      result.then((test) => {
        assert.deepEqual(test, "Hello World!?!");
        done();
      });
    });
  });
});
