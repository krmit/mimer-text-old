import { assert } from "chai";
import { t, Text, TextToWebSync } from "../src/index";

describe("Web output results in strings", () => {
  let hello: Text;

  beforeEach(function () {
    hello = new Text(["Hello", " ", "World", "!"]);
  });

  describe("Simple test of TextToWeb", () => {
    it('Simpel HTML representation of the text "Hello World"', () => {
      const result = TextToWebSync(hello);
      assert.deepEqual(result, "<span>Hello World!</span>");
    });
    it("Apend a new text", () => {
      const result = TextToWebSync(hello.add(t("?", "!")));
      assert.deepEqual(result, "<span>Hello World!<span>?!</span></span>");
    });
  });
});
