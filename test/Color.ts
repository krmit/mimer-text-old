import { testText } from "../src/testFunctions";
import { t, Text, TextToJson } from "../src/index";
import colors from "colors/safe";

describe("Text with some colors", () => {
  describe("Simple color tests", () => {
    it("Text with a red word", () => {
      const result = t("Hello ", t("World").red, "!");
      testText(result, {
        string: "Hello World!",
        terminal: "Hello " + colors.red("World") + "!",
        web: '<span>Hello <span style="color:red">World</span>!</span>',
      });
    });
    it("Text with a blue word", () => {
      const result = t("Hello ", t("World").blue, "!");
      testText(result, {
        string: "Hello World!",
        terminal: "Hello " + colors.blue("World") + "!",
        web: '<span>Hello <span style="color:blue">World</span>!</span>',
      });
    });
  });
});
