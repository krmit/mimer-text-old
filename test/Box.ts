import { testText } from "../src/testFunctions";
import { t, Text, TextToJson } from "../src/index";

describe("toJSON with Box propries sush as border", () => {
  describe("Simple boxes with borders", () => {
    it("Default box around a text with one line", () => {
      const result = t("This is a test!").border;
      testText(result, {
        terminal: ` ┌───────────────┐
 │This is a test!│ 
 └───────────────┘
`,
        web: '<span style="border-style:solid; padding:5px; margin:5px">This is a test!</span>',
      });
    });
    it("Default box around a text with two lines", () => {
      const result = t("This is anouther test!\nWith more lines").border;
      testText(result, {
        terminal: ` ┌──────────────────────┐
 │This is anouther test!│ 
 │With more lines       │ 
 └──────────────────────┘
`,
        web: `<span style="border-style:solid; padding:5px; margin:5px">This is anouther test!
With more lines</span>`,
      });
    });
  });
  it("Default box around a text with two lines", () => {
    const result = t(
      "This is anouther ",
      t("test!").cyan.underline,
      "\nWith ",
      t("colors ").green,
      "lines"
    ).border;
    testText(result, {
      terminal: ` ┌──────────────────────┐
 │This is anouther \u001b[4m\u001b[36mtest!\u001b[39m\u001b[24m│ 
 │With \u001b[32mcolors \u001b[39mlines     │ 
 └──────────────────────┘
`,
      web: `<span style="border-style:solid; padding:5px; margin:5px">This is anouther <span style="color:cyan; text-decoration:underline">test!</span>
With <span style="color:green">colors </span>lines</span>`,
    });
  });
});
