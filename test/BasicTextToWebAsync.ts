import { assert } from "chai";
import { t, Text, TextToWeb } from "../src/index";

describe("TextToWebAsync", () => {
  let hello: Text;

  beforeEach(function () {
    hello = new Text(["Hello", " ", "World", "!"]);
  });

  describe("Simple test with promise", () => {
    it('A simpel "Hello World" text', (done) => {
      const result = TextToWeb(hello);
      result.then((test) => {
        assert.deepEqual(test, "<span>Hello World!</span>");
        done();
      });
    });
    it("Apend a new text", (done) => {
      const result = TextToWeb(hello.add(t("?", "!")));
      result.then((test) => {
        assert.deepEqual(test, "<span>Hello World!<span>?!</span></span>");
        done();
      });
    });
  });
});
