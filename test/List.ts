import { testText } from "../src/testFunctions";
import { t } from "../src/index";

describe("toJSON with List properties", () => {
  describe("Test of list", () => {
    it("A simpel list", () => {
      const result = t("Item A!", "Item B?", "Item C++").list;
      testText(result, {
        string: "Item A!\nItem B?\nItem C++\n",
        web: `<ul>
    <li> Item A! </li>
    <li> Item B? </li>
    <li> Item C++ </li>
</ul>`,
      });
    });
    it("A simpel list", () => {
      const result = t(
        t("Item ", "A!"),
        t("Item ", "B?"),
        t("Item ", "C++")
      ).list;
      testText(result, {
        string: "Item A!\nItem B?\nItem C++\n",
        /* Not perfect! Space shoul be the same as in previus test */
        web: `<ul>
    <li>Item A!</li>
    <li>Item B?</li>
    <li>Item C++</li>
</ul>`,
      });
    });
  });

  describe("Test of orderedList", () => {
    it("A simpel orderedList", () => {
      const result = t("Item A!", "Item B?", "Item C++").orderedList;
      testText(result, {
        string: " 1. Item A!\n 2. Item B?\n 3. Item C++\n",
        web: `<ol>
    <li> Item A! </li>
    <li> Item B? </li>
    <li> Item C++ </li>
</ol>`,
      });
    });
    it("A simpel ordered list", () => {
      const result = t(
        t("Item ", "A!"),
        t("Item ", "B?"),
        t("Item ", "C++")
      ).orderedList;
      testText(result, {
        string: " 1. Item A!\n 2. Item B?\n 3. Item C++\n",
        web: `<ol>
    <li>Item A!</li>
    <li>Item B?</li>
    <li>Item C++</li>
</ol>`,
      });
    });
    it("A simpel ordered list with mixed types", () => {
      const result = t(
        t("Item ", "A!"),
        "Item B?",
        t("Item ", "C++")
      ).orderedList;
      testText(result, {
        string: " 1. Item A!\n 2. Item B?\n 3. Item C++\n",
        web: `<ol>
    <li>Item A!</li>
    <li> Item B? </li>
    <li>Item C++</li>
</ol>`,
      });
    });
  });
  it("A simpel ordered list with mixed types and styles", () => {
    const result = t(
      t("Item ", "A!").red,
      "Item B?",
      t("Item ", t("C++").bold)
    ).orderedList;
    testText(result, {
      string: " 1. Item A!\n 2. Item B?\n 3. Item C++\n",
      terminal:
        " 1. \u001b[31mItem \u001b[39m\u001b[31mA!\u001b[39m\n 2. Item B?\n 3. Item \u001b[1mC++\u001b[22m\n",
      web: `<ol>
    <li style="color:red">Item A!</li>
    <li> Item B? </li>
    <li>Item <span style="font-weight:bold">C++</span></li>
</ol>`,
    });
  });
});
