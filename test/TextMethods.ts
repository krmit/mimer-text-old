import { assert } from "chai";
import { t, Text, TextToTerminalSync } from "../src/index";
import { testText } from "../src/testFunctions";

describe("Test method on Text", () => {
  let hello: Text;

  beforeEach(function () {
    hello = new Text(["Hello", " ", "World", "!"]);
  });

  describe("Class", () => {
    it("Adding a class", () => {
      hello.addClass("content");
      hello.addClass("center");
      assert.deepEqual(hello.Class, ["content", "center"]);
    });
    it("Has a class", () => {
      hello.addClass("content");
      hello.addClass("center");
      const result = hello.hasClass("content");
      assert.deepEqual(result, true);
    });
    it("JSON representation", () => {
      hello.addClass("content");
      hello.addClass("center");
      testText(hello);
    });
  });
});
