"use strict";
import { TextTo, Level, Line, Config } from "../TextTo";
import { doList, doBox, doFixed, doPercent, doSpan } from "../stringTools";
import { Text } from "../index";
import {
  COLOR,
  STYLE,
  SeqLink,
  Seq,
  RESET,
} from "../../terminal-sequences/main";

export interface TerminaLevel {
  style: boolean;
}

export function TextToTerminalSync(
  text: Text,
  config: Config = {
    indent: 0,
  }
): string {
  return TextTo<string, TerminaLevel, {}>(
    text,
    config,
    (text: Text, a: string, level: Level<TerminaLevel>, line: Line<{}>) => a,
    (
      text: Text,
      a: string,
      b: string,
      level: Level<TerminaLevel>,
      line: Line<{}>
    ) => a + b,
    terminalEndSync,
    terminalStartSync
  );
}

export function terminalStartSync(
  text: Text,
  level: Level<TerminaLevel>,
  line: Line<{}>
) {
  let result = "";
  if (level.info === null) {
    level.info = { style: false };
  }

  /* ### Get options from text node ### */

  /* To theme */
  if (text.hasTag("headline")) {
    text.TextDecoration = "underline";
    text.Newline = true;
  }

  if (text.hasTag("important")) {
    text.Color = "red";
  }

  /* End to theme */

  switch (text.ListStyle) {
    case "list":
      level.newline = true;
      break;
    case "list-ordered":
      level.newline = true;
      level.itemType = "list-ordered";
      level.itemSpan = Math.floor(Math.log10(text.Data.length)) + 3;
      break;
    case "list-unordered":
      level.newline = true;
      level.itemType = "list-unordered";
      break;
  }

  /* ### Make style for text ### */

  let styles: (string | number)[] = [];

  if (text.Color) {
    styles.push(COLOR[text.Color]);
  }

  if (text.FontWeight) {
    styles.push(STYLE[text.FontWeight]);
  }

  if (text.TextDecoration) {
    if (text.TextDecoration === "line-through") {
      styles.push(STYLE["strikethrough"]);
    } else if (text.TextDecoration === "overline") {
      styles.push(STYLE["overline"]);
    } else {
      styles.push(STYLE[text.TextDecoration]);
    }
  }

  /* This kod is for prompt
    let result: string;
  result = TextToTerminalSync(text);

  if (text.PromptForm !== null) {
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    });

    let p: Promise<Answer> = new Promise((resolve, reject) => {
      rl.question(result, (answer: string) => {
        rl.close();
        resolve({ status: "ok", data: answer });
      });
    });
    return p;
  } else {
    console.log(result);
  }

  return Promise.resolve({ status: "ok" });
*/
  if (styles.length > 0) {
    level.info.style = true;
    return Seq(...styles);
  } else {
    return "";
  }
}

export function terminalEndSync(
  text: Text,
  msg: string,
  level: Level<TerminaLevel>,
  line: Line<{}>
) {
  let result = msg;

  /* ### Make style for text ### */

  if (text.NumberFixed && text.NumberFixed > 0) {
    result = doFixed(result, text.NumberFixed);
  }

  if (text.NumberForm === "percent") {
    result = doPercent(result);
  }

  if (text.Span && text.Span > 0) {
    result = doSpan(result, result.length, text.Span);
  }

  if (text.BorderStyle) {
    result = doBox(result, level);
  }

  if (text.Link !== null) {
    result = SeqLink(text.Link, result);
  }

  if (level.info !== null && level.info.style) {
    result += RESET;
  }

  result = doList(result, level);
  result = text.Newline ? result + "\n" : result;
  return result;
}
