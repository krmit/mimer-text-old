"use strict";
import { TextTo, Level, Line, Config } from "../TextTo";
import { doList, doBox, doFixed, doPercent, doSpan } from "../stringTools";
import { Text } from "../index";
import colors from "colors/safe";
import {
  terminalStartSync,
  terminalEndSync,
  TerminaLevel,
} from "./TextToTerminalSync";
import { JsonToTextSync } from "../Json/JsonToTextSync";
import fetch from "cross-fetch";
import fs from "fs-extra";
import { CLIENT } from "../../config";

export function TextToTerminal(
  text: Text,
  config: Config = {
    indent: 0,
  }
) {
  return TextTo<Promise<string>, TerminaLevel, {}>(
    text,
    config,
    (text: Text, a: string, level: Level<TerminaLevel>, line: Line<{}>) =>
      Promise.resolve(a),
    terminalAdd,
    terminalEnd
  );
}

export function terminalAdd(
  text: Text,
  a: Promise<string>,
  b: Promise<string>,
  level: Level<TerminaLevel>,
  line: Line<{}>
): Promise<string> {
  return Promise.all([a, b]).then((data) => data[0] + data[1]);
}

export async function terminalEnd(
  text: Text,
  msg: Promise<string>,
  level: Level<TerminaLevel>,
  line: Line<{}>
): Promise<string> {
  return msg.then((m) => {
    /* Not implmetet yet */
    if (text.Image !== null) {
      text = text.border;
    }

    let next_string: Promise<string>;
    if (text.Source !== null) {
      let url = text.Source;
      if (CLIENT) {
        next_string = fetch(url).then((respons) => respons.text());
      } else {
        next_string = fs.readFile(url, "utf-8");
      }
      next_string = next_string.then((content) => {
        const json = JSON.parse(content);
        let r = TextToTerminal(JsonToTextSync(json));
        return r;
      });

      return next_string;
    }

    let result = terminalEndSync(text, m, level, line);
    return Promise.resolve(result);
  });
}
