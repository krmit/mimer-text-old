import { Level } from "./TextTo";

export function doSpan(text: string, length: number, span: number): string {
  if (span < length) {
    if (length > 6) {
      // This will be buggy if text contains escape charaters.
      return text.substring(0, span - 3) + "...";
    } else {
      return text.substring(0, span);
    }
  } else if (span !== length) {
    return text + " ".repeat(span - length);
  } else {
    return text;
  }
}

export function doFixed(text: string, fix: number): string {
  let result = Number(text);
  if (!isNaN(result)) {
    return String(result.toFixed(fix));
  } else {
    return text;
  }
}

export function doPercent(text: string) {
  let result = Number(text);
  if (!isNaN(result)) {
    return String(result * 100) + "%";
  } else {
    return text;
  }
}

/* Any should be removed. */
export function doList(msg: string, level: Level<any>) {
  let result = "";
  if (level.itemType === "list-ordered") {
    let line_number = String(level.index + 1) + ".";
    result +=
      " " + doSpan(line_number, line_number.length, level.itemSpan) + msg;
  } else if (level.itemType === "list-unordered") {
    result += " * " + msg;
  } else {
    result += msg;
  }
  return result;
}

export function doBox(text: string, config: any) {
  let result = "";
  const box: any = {};
  box.top = config.borderTop ? config.borderTop : "─";
  box.right = config.borderRight ? config.borderRight : "│ ";
  box.bottom = config.borderBottom ? config.borderBottom : "─";
  box.left = config.borderLeft ? config.borderLeft : " │";
  box.topLeft = config.borderTopLeft ? config.borderTopLeft : " ┌";
  box.topRight = config.borderTopRight ? config.borderTopRight : "┐";
  box.bottomLeft = config.borderBottomRight ? config.borderBottomRight : " └";
  box.bottomRight = config.borderBottomLeft ? config.borderBottomLeft : "┘";

  const lines = text.split("\n");
  box.width = lines.reduce((a: string, b: string) => {
    const l = onlyText(b).length;
    return Number(a) > l ? a : String(l);
  }, "0");

  box.height = lines.length;
  result += box.topLeft;
  result += box.top.repeat(box.width);
  result += box.topRight;
  result += "\n";

  for (let i = 0; i < box.height; i++) {
    result += box.left;
    result += lines[i] + " ".repeat(box.width - onlyText(lines[i]).length);
    result += box.right;
    result += "\n";
  }

  result += box.bottomLeft;
  result += box.bottom.repeat(box.width);
  result += box.bottomRight;
  result += "\n";
  return result;
}

// From https://stackoverflow.com/a/29497680
function onlyText(text: string) {
  return text.replace(
    /[\u001b\u009b][[()#;?]*(?:[0-9]{1,4}(?:;[0-9]{0,4})*)?[0-9A-ORZcf-nqry=><]/g,
    ""
  );
}

function printEscape(text: string) {
  return text.replace(/[\u001b\u009b]/g, "\\\u001b\\u009b");
}
