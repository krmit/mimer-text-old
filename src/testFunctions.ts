"use strict";

import { assert } from "chai";
import eql from "deep-eql";
import {
  Text,
  TextToTerminalSync,
  TextToStringSync,
  TextToJsonSync,
  TextToWebSync,
  JsonToTextSync,
} from "./index";

interface testTextType {
  string?: string;
  terminal?: string;
  web?: string;
}

export function testText(text: Text, testText?: testTextType) {
  assert.deepEqual(
    JsonToTextSync(TextToJsonSync(text)),
    text,
    "Error with JSON converting"
  );

  if (testText !== undefined) {
    if (testText.string !== undefined) {
      const result: string = TextToStringSync(text);
      assert.equal(testText.string, result, "Error with String");
    }

    if (testText.terminal !== undefined) {
      const result = TextToTerminalSync(text);
      assert.equal(testText.terminal, result, "Error with Terminal");
    }

    if (testText.web !== undefined) {
      const result = TextToWebSync(text);
      assert.equal(testText.web, result, "Error with Web");
    }
  }
}
