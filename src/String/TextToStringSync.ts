"use strict";
import { TextTo, Level, Line, Config } from "../TextTo";
import { doSpan, doList } from "../stringTools";
import { Text } from "../index";

export function TextToStringSync(text: Text, config: Config = {}): string {
  return TextTo<string>(
    text,
    config,
    (text: Text, a: string, level: Level<{}>, line: Line<{}>) => a,
    (text: Text, a: string, b: string, level: Level<{}>, line: Line<{}>) =>
      a + b,
    (text: Text, a: string, level: Level<{}>, line: Line<{}>) => a,
    stringStartSync
  );
}

export function stringStartSync(text: Text, level: Level<{}>, line: Line<{}>) {
  let result = "";

  switch (text.ListStyle) {
    case "list":
      level.newline = true;
      break;
    case "list-ordered":
      level.newline = true;
      level.itemType = "list-ordered";
      level.itemSpan = Math.floor(Math.log10(text.Data.length)) + 3;
      break;
    case "list-unordered":
      level.newline = true;
      level.itemType = "list-unordered";
      break;
  }

  return result;
}

export function stringAdd(
  text: Text,
  a: string,
  b: string,
  level: Level<{}>,
  line: Line<{}>
) {
  let result = a;
  result =
    text.Span && text.Span > 0
      ? doSpan(result, result.length, text.Span)
      : result;
  result += doList(b, level);
  result = level.newline ? result + "\n" : result;
  return result;
}

export function stringEndSync(text: Text, msg: string, config: Config) {
  let result = msg;
  return (result = text.Newline && text.Newline ? result + "\n" : result);
}
