/**
 * Shorhand function to easy create a Text objet.
 *
 * @params messages A list of strings or [[Text]]
 * @return A new text that can be modified by the methods of Text.
 */

export function t(...messages: (string | number | Text)[]) {
  return new Text(messages);
}

export * from "./String/TextToStringSync";
export * from "./Terminal/TextToTerminalSync";
export * from "./Terminal/TextToTerminal";
export * from "./Json/TextToJsonSync";
export * from "./Json/JsonToTextSync";
export * from "./Web/TextToWebSync";
export * from "./Web/TextToWeb";

// Deklaration of common types
export type colorType =
  | "red"
  | "blue"
  | "green"
  | "yellow"
  | "gray"
  | "cyan"
  | "magenta";
export type tagType = "headline" | string;
export type fontWeightType = "bold";
export type textDecorationType = "underline" | "overline" | "line-through";
export type listStyleType = "list" | "list-ordered" | "list-unordered";
export type borderStyleType = "solid";
export type promptFormType = "normal";
export type numberFormType = "percent";

export function isColor(color: string): color is colorType {
  return ["red", "blue", "green", "yellow", "gray", "cyan", "magenta"].includes(
    color
  );
}

export function isFontWeight(weight: string): weight is fontWeightType {
  return ["bold"].includes(weight);
}

export function isTextDecoration(
  decoration: string
): decoration is textDecorationType {
  return ["underline", "overline", "line-through"].includes(decoration);
}

export function isListStyle(style: string): style is listStyleType {
  return ["list", "list-ordered", "list-unordered"].includes(style);
}

export function isBorderStyle(style: string): style is borderStyleType {
  return ["solid"].includes(style);
}

export function isPromptForm(style: string): style is promptFormType {
  return ["normal"].includes(style);
}

export function isNumberForm(style: string): style is numberFormType {
  return ["normal"].includes(style);
}

export interface Answer {
  status: "" | "ok";
  data?: string;
}

export type URL = string;

/**
 * The Text class represent text with styles.
 */
export class Text {
  Color?: colorType;
  FontWeight?: fontWeightType;
  TextDecoration?: textDecorationType;
  NumberFixed?: number;
  NumberForm?: numberFormType;
  Link: URL | null = null;
  Image: URL | null = null;
  Theme?: string;
  Class: string[] | null = null;
  ListStyle: listStyleType | null = null;
  BorderStyle: borderStyleType | null = null;
  PromptForm: promptFormType | null = null;
  Newline: boolean | null = null;
  Span: number | null = null;
  Tags: string[] = [];
  Data: (string | Text)[];
  Block: boolean | null = null;
  Code: string | null = null;
  Math: boolean | null = null;
  Source: URL | null = null;

  /**
   * Create a Text from parameters.
   *
   * @param messages A list of strings or [[Text]]
   */

  constructor(messages: (string | number | Text)[] | string | Text = []) {
    this.Data = [];
    this.Tags = [];

    if (Array.isArray(messages)) {
      for (let message of messages) {
        this.add(message);
      }
    } else {
      this.Data[0] = messages;
    }
  }

  add(message: string | number | Text) {
    if (typeof message === "number") {
      message = String(message);
    }
    this.Data.push(message);
    if (message instanceof Text) {
      Object.setPrototypeOf(message, this);
    }
    return this;
  }

  theme(themeName: string) {
    this.Theme = themeName;
    return this;
  }

  addTag(tag: tagType) {
    this.Tags.push(tag);
    return this;
  }

  hasTag(tag: tagType) {
    return this.Tags.indexOf(tag) >= 0;
  }

  addClass(tag: string) {
    if (this.Class !== null) {
      this.Class.push(tag);
    } else {
      this.Class = [tag];
    }
    return this;
  }

  hasClass(tag: string) {
    if (this.Class === null) {
      return false;
    } else {
      return this.Class.indexOf(tag) >= 0;
    }
  }

  span(span: number) {
    this.Span = span;
    return this;
  }

  fixed(quantity: number) {
    this.NumberFixed = quantity;
    return this;
  }

  link(url: string) {
    this.Link = url;
    return this;
  }

  image(url: string) {
    this.Image = url;
    return this;
  }

  code(lang: string) {
    this.Code = lang;
    return this;
  }

  source(url: URL) {
    this.Source = url;
    return this;
  }

  get newline() {
    this.Newline = true;
    return this;
  }

  get block() {
    this.Block = true;
    return this;
  }

  get math() {
    this.Math = true;
    return this;
  }

  get headline() {
    this.Tags.push("headline");
    return this;
  }

  get important() {
    this.Tags.push("important");
    return this;
  }

  get red() {
    this.Color = "red";
    return this;
  }

  get blue() {
    this.Color = "blue";
    return this;
  }

  get green() {
    this.Color = "green";
    return this;
  }

  get yellow() {
    this.Color = "yellow";
    return this;
  }

  get magenta() {
    this.Color = "magenta";
    return this;
  }

  get cyan() {
    this.Color = "cyan";
    return this;
  }

  get gray() {
    this.Color = "gray";
    return this;
  }

  get bold() {
    this.FontWeight = "bold";
    return this;
  }

  get underline() {
    this.TextDecoration = "underline";
    return this;
  }

  get lineThrough() {
    this.TextDecoration = "line-through";
    return this;
  }

  get overline() {
    this.TextDecoration = "overline";
    return this;
  }

  get list() {
    this.ListStyle = "list";
    return this;
  }

  get orderedList() {
    this.ListStyle = "list-ordered";
    return this;
  }

  get unorderedList() {
    this.ListStyle = "list-unordered";
    return this;
  }

  get border() {
    this.BorderStyle = "solid";
    return this;
  }

  get percent() {
    this.NumberForm = "percent";
    return this;
  }

  get prompt() {
    this.PromptForm = "normal";
    return this;
  }
}
