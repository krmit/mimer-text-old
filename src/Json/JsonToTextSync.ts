"use strict";
import { TextTo } from "../TextTo";
import {
  Text,
  isColor,
  isFontWeight,
  isTextDecoration,
  isListStyle,
  isBorderStyle,
  isPromptForm,
  isNumberForm,
} from "../index";

interface TextJsonType {
  color?: string;
  fontWeight?: string;
  textDecoration?: string;
  listStyle?: string;
  link?: string;
  class?: string[];
  borderStyle?: string;
  promptForm?: string;
  numberForm?: string;
  newLine?: boolean;
  spanLength?: number;
  numberFixed?: number;
  themeName?: string;
  tags?: string[];
  data?: (string | TextJsonType)[];
}

export function JsonToTextSync(data: any): Text {
  const result = new Text();

  let item: string | undefined;
  let any_item: any;

  item = data.color;
  if (item) {
    if (isColor(item)) {
      result.Color = item;
    } else {
      throw String(item) + " is not a text color.";
    }
  }

  item = data.fontWeight;
  if (item) {
    if (isFontWeight(item)) {
      result.FontWeight = item;
    } else {
      throw String(item) + " is not a font Weight.";
    }
  }

  item = data.textDecoration;
  if (item) {
    if (isTextDecoration(item)) {
      result.TextDecoration = item;
    } else {
      throw String(item) + " is not a text decoration.";
    }
  }

  item = data.listStyle;
  if (item) {
    if (isListStyle(item)) {
      result.ListStyle = item;
    } else {
      throw String(item) + " is not a list style.";
    }
  }

  item = data.borderStyle;
  if (item) {
    if (isBorderStyle(item)) {
      result.BorderStyle = item;
    } else {
      throw String(item) + " is not a border style.";
    }
  }

  item = data.promptForm;
  if (item) {
    if (isPromptForm(item)) {
      result.PromptForm = item;
    } else {
      throw String(item) + " is not a prompt form.";
    }
  }

  item = data.numberForm;
  if (item) {
    if (isNumberForm(item)) {
      result.NumberForm = item;
    } else {
      throw String(item) + " is not a prompt form.";
    }
  }

  any_item = data.newline;
  if (any_item) {
    if (typeof any_item === "boolean") {
      result.Newline = any_item;
    } else {
      throw String(any_item) + " is not a boolean for a new line.";
    }
  }

  any_item = data.block;
  if (any_item) {
    if (typeof any_item === "boolean") {
      result.Block = any_item;
    } else {
      throw String(any_item) + " is not a boolean for a block.";
    }
  }

  any_item = data.math;
  if (any_item) {
    if (typeof any_item === "boolean") {
      result.Math = any_item;
    } else {
      throw String(any_item) + " is not a boolean for math.";
    }
  }

  any_item = data.spanLength;
  if (any_item) {
    if (typeof any_item === "number") {
      result.Span = any_item;
    } else {
      throw String(any_item) + " is not a number for length of span.";
    }
  }

  any_item = data.numberFixed;
  if (any_item) {
    if (typeof any_item === "number") {
      result.NumberFixed = any_item;
    } else {
      throw (
        String(any_item) + " is not a number for fixed decimal of a number."
      );
    }
  }

  any_item = data.themeName;
  if (any_item) {
    if (typeof any_item === "string") {
      result.Theme = any_item;
    } else {
      throw String(any_item) + " is not a possibel name for a theme";
    }
  }

  any_item = data.link;
  if (any_item) {
    console.log(data.link);
    if (typeof any_item === "string") {
      result.Link = any_item;
    } else {
      throw String(any_item) + " is not a possibel URL for a link";
    }
  }

  any_item = data.image;
  if (any_item) {
    if (typeof any_item === "string") {
      result.Image = any_item;
    } else {
      throw String(any_item) + " is not a possibel URL for a image";
    }
  }

  any_item = data.source;
  if (any_item) {
    if (typeof any_item === "string") {
      result.Source = any_item;
    } else {
      throw String(any_item) + " is not a possibel URL for a source text";
    }
  }

  any_item = data.code;
  if (any_item) {
    if (typeof any_item === "string") {
      result.Code = any_item;
    } else {
      throw String(any_item) + " is not a possibel name of a coding language";
    }
  }

  any_item = data.tags;
  if (any_item) {
    if (Array.isArray(any_item)) {
      result.Tags = any_item;
    } else {
      throw String(any_item) + " is not a list of tags";
    }
  }

  any_item = data.class;
  if (any_item) {
    if (Array.isArray(any_item)) {
      result.Class = any_item;
    } else {
      throw String(any_item) + " is not a list of classes";
    }
  }

  if (data.data) {
    for (let d of data.data) {
      if (typeof d === "string") {
        result.add(d);
      } else {
        result.add(JsonToTextSync(d));
      }
    }
  }

  return result;
}
