"use strict";
import { TextTo, Level, Line, Config } from "../TextTo";
import { Text } from "../index";

export function TextToJsonSync(text: Text, config: Config = {}): any {
  return TextTo<any>(text, config, jsonConverter, jsonAdd);
}

export function jsonConverter(
  text: Text,
  msg: string,
  level: Level<{}>,
  line: Line<{}>
): any {
  const result: any = {};
  if (text.Color) {
    result["color"] = text.Color;
  }
  if (text.FontWeight) {
    result["fontWeight"] = text.FontWeight;
  }
  if (text.TextDecoration) {
    result["textDecoration"] = text.TextDecoration;
  }
  if (text.ListStyle) {
    result["listStyle"] = text.ListStyle;
  }
  if (text.BorderStyle) {
    result.borderStyle = text.BorderStyle;
  }
  if (text.PromptForm !== null) {
    result.promptForm = text.PromptForm;
  }
  if (text.Theme) {
    result["theme"] = text.Theme;
  }
  if (text.Link !== null) {
    result["link"] = text.Link;
  }
  if (text.Image !== null) {
    result["image"] = text.Image;
  }
  if (text.Source !== null) {
    result["source"] = text.Source;
  }
  if (text.Span && text.Span > 0) {
    result.span = text.Span;
  }
  if (text.NumberForm) {
    result.numberForm = text.NumberForm;
  }
  if (text.NumberFixed && text.NumberFixed > 0) {
    result.numberFixed = text.NumberFixed;
  }
  if (text.Newline === true) {
    result.newline = true;
  }
  if (text.Block === true) {
    result.block = true;
  }
  if (text.Math === true) {
    result.math = true;
  }
  if (text.Code) {
    result.code = text.Code;
  }
  if (text.Class !== null && text.Class.length > 0 && msg === "") {
    result.class = Object.assign([], text.Class);
  }
  if (text.Tags.length > 0 && msg === "") {
    result.tags = Object.assign([], text.Tags);
  }
  if (msg !== "") {
    result.data = [msg];
  } else {
    result.data = [];
  }

  return result;
}

export function jsonAdd(
  text: Text,
  before: any,
  after: any,
  level: Level<{}>,
  line: Line<{}>
): any {
  let result: any = {};
  for (let property in after) {
    if (before[property] !== after[property]) {
      result[property] = after[property];
    }
  }
  if (
    Object.keys(result).length === 1 &&
    result.data.length === 1 &&
    typeof result.data[0] === "string"
  ) {
    before.data.push(result.data[0]);
  } else {
    before.data.push(result);
  }
  return before;
}
