"use strict";
import { doSpan } from "./stringTools";
import { Text, t } from "./index";
import { TextToTerminal } from "./Terminal/TextToTerminal";
import {
  BLUE,
  START,
  RESET,
  BOLD,
  END,
  DeSeq,
} from "../terminal-sequences/main";

enum DEBUG {
  fatal,
  error,
  info,
  warn,
  debug,
  trace,
}

let DEBUG_INDENT = 0;

const debug_level = DEBUG.trace;

/**
 *  Configuration for the rendering of the Text object.
 *
 */
export interface Config {
/** How mutch a new line in generated code will be indent */
  indent?: number;
}

/**
 *  Configuration following the level of the node. 
 *
 */
export interface Level<L> {
  newline: boolean;
  index: number;
  deep: number;
  firstOnLine: boolean;
  info: L | null;
  itemType: string;
  itemSpan: number;
}

/**
 *  Configuration following the generatet code. 
 *
 */
export interface Line<N> {
  indent: number;
  info: N | null;
}

/**
 *  Start the rendering of a Text object 
 *
 */
export function TextTo<T, L = {}, N = {}>(
  text: Text,
  config: Config,
  converter: (text: Text, msg: string, level: Level<L>, line: Line<N>) => T,
  add: (text: Text, a: T, b: T, level: Level<L>, line: Line<N>) => T,
  end = (text: Text, msg: T, level: Level<L>, line: Line<N>): T => msg,
  start = (text: Text, level: Level<L>, line: Line<N>): T =>
    converter(text, "", level, line),
  begin = (text: Text, level: Level<L>, line: Line<N>) =>
    converter(text, "", level, line),
  finish = (text: Text, msg: T, level: Level<L>, line: Line<N>) => msg
) {
  let line: Line<N> = {
    indent: config.indent ?? 0,
    info: null,
  };

  let level: Level<L> = {
    deep: 0,
    newline: false,
    index: 0,
    firstOnLine: true,
    info: null,
    itemType: "",
    itemSpan: 2,
  };

  let begining = begin(text, level, line);
  let result = NodeTo<T, L, N>(text, converter, add, end, start, level, line);
  result = add(text, begining, result, level, line);
  result = finish(text, result, level, line);
  return result;
}

/**
 *  Rendering each node. 
 *
 */
export function NodeTo<T, L = {}, N = {}>(
  text: Text,
  converter: (text: Text, msg: string, level: Level<L>, line: Line<N>) => T,
  add: (text: Text, a: T, b: T, level: Level<L>, line: Line<N>) => T,
  end = (text: Text, msg: T, level: Level<L>, line: Line<N>): T => msg,
  start = (text: Text, level: Level<L>, line: Line<N>): T =>
    converter(text, "", level, line),
  level: Level<L>,
  line: Line<N>
): T {
  /* ### Start node ### */

  /* Configurate this node by updating level */
  textLog("start", text);
  DEBUG_INDENT++;

  let new_level: Level<L> = Object.assign(
    { index: 0, itemType: "", newline: false },
    level
  );
  new_level.deep++;

  /* Start rendering of node */
  let result = start(text, new_level, line);
  resultLog<T>("result", result);

  /* ### For evry child node ### */
  for (const message of text.Data) {
    if (message !== "") {
      let tmp: T;
      if (typeof message === "string") {
        tmp = converter(text, message, level, line);
      } else {
        tmp = NodeTo<T, L, N>(
          message,
          converter,
          add,
          end,
          start,
          new_level,
          line
        );
      }
      result = add(text, result, tmp, new_level, line);
      /* Update for next element */
      new_level.index++;
    }
  }

  /* ### End node ### */
  DEBUG_INDENT--;
  textLog("end", text);
  result = end(text, result, new_level, line);
  return result;
}

/**
 *  Logg a message representing importent things for a Text object
 *
 * @param text the Text object to be logged.
 */
function textLog(msg: string, text: Text) {
  if (debug_level >= DEBUG.debug) {
    let result = " ".repeat(DEBUG_INDENT);
    result += ">>>> " + msg + " # ";
    let property: keyof Text;
    const list_of_properties = [];
    for (property in text) {
      const value = text[property];
      if (
        value !== null &&
        value !== undefined &&
        !(Array.isArray(value) && value.length === 0)
      ) {
        if (property === "Data") {
          let data_text = "";
          for (const data of value as string[]) {
            if (typeof data === "string") {
              data_text += data;
            } else {
              data_text += "??";
            }
          }
          list_of_properties.unshift(START + BOLD + END + data_text + RESET);
        } else {
          list_of_properties.push(
            START + BLUE + END + property + ": " + RESET + value
          );
        }
      }
    }
    result += list_of_properties.join(" ");
    result += " <<<<";
    console.log(result);
  }
}

/**
 *  Logg a message for any type. If string tried to log as terminal output.
 *
 * @param result value of type T.
 */
function resultLog<T>(msg: string, result: T) {
  if (debug_level >= DEBUG.debug) {
    if (typeof result === "string") {
      console.log("  ".repeat(DEBUG_INDENT) + msg, DeSeq(result));
    }
  }
}
