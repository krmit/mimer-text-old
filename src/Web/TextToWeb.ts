"use strict";
import { TextTo, Config, Level, Line } from "../TextTo";
import { Text } from "../index";
import {
  webConverter,
  webAdd,
  webEnd,
  webStart,
  WebLevel,
  WebLine,
} from "./TextToWebSync";
import { JsonToTextSync } from "../Json/JsonToTextSync";
import fetch from "cross-fetch";
import fs from "fs-extra";
import { CLIENT } from "../../config";

const ESC = "\x1b";
const BELL = ESC + "\\";
const LINK = ESC + "]8;;";

export function TextToWeb(
  text: Text,
  config: Config = {
    indent: 0,
  }
) {
  return TextTo<Promise<string>, WebLevel, WebLine>(
    text,
    config,
    (text: Text, a: string, level: Level<WebLevel>, line: Line<WebLine>) =>
      Promise.resolve(a),
    webAsyncAdd,
    webAsyncEnd,
    webAsyncStart
  );
}

export function webAsyncStart(
  text: Text,
  level: Level<WebLevel>,
  line: Line<WebLine>
) {
  return Promise.resolve(webStart(text, level, line));
}

export async function webAsyncConverter(
  text: Text,
  msg: string,
  level: Level<WebLevel>,
  line: Line<WebLine>
) {
  return Promise.resolve(webConverter(text, msg, level, line));
}

export function webAsyncAdd(
  text: Text,
  a: Promise<string>,
  b: Promise<string>,
  level: Level<WebLevel>,
  line: Line<WebLine>
) {
  return Promise.all([a, b]).then((data) =>
    webAdd(text, data[0], data[1], level, line)
  );
}

export async function webAsyncEnd(
  text: Text,
  msg: Promise<string>,
  level: Level<WebLevel>,
  line: Line<WebLine>
) {
  return msg.then((m) => {
    if (text.Image !== null) {
      text = text.border;
    }

    let next_string: Promise<string>;
    if (text.Source !== null) {
      let url = text.Source;
      if (CLIENT) {
        next_string = fetch(url).then((respons) => respons.text());
      } else {
        next_string = fs.readFile(url, "utf-8");
      }
      next_string = next_string.then((content) => {
        const json = JSON.parse(content);
        let r = TextToWeb(JsonToTextSync(json));
        return r;
      });

      return next_string;
    }

    let result = webEnd(text, m, level, line);
    return Promise.resolve(result);
  });
}
