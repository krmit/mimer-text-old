"use strict";
import { TextTo, Config, Level, Line } from "../TextTo";
import { doFixed, doPercent } from "../stringTools";
import { TextToStringSync } from "../String/TextToStringSync";
import { Text } from "../index";

export type Element =
  | "div"
  | "span"
  | "ul"
  | "ol"
  | "li"
  | "strong"
  | "p"
  | "h1"
  | "h2"
  | "code";

export interface WebLevel {
  style: boolean;
  element: Element;
  firstOnLine: boolean;
}

export interface WebLine {
  nextElement?: Element;
}

export function TextToWebSync(text: Text, config: Config = {}): string {
  return TextTo<string, WebLevel, WebLine>(
    text,
    config,
    webConverter,
    webAdd,
    webEnd,
    webStart
  );
}

export function webConverter(
  text: Text,
  msg: string,
  level: Level<WebLevel>,
  line: Line<WebLine>
) {
  let result: string;
  if (level.info !== null) {
    if (
      level.info.element !== undefined &&
      ["ul", "ol"].includes(level.info.element)
    ) {
      const indent = (line.indent + 1) * 4;
      let element = "li";
      result =
        " ".repeat(indent) +
        "<" +
        element +
        "> " +
        msg +
        " </" +
        element +
        ">\n";
    } else {
      result = msg;
    }
  } else {
    result = msg;
  }
  return result;
}

export function webStart(
  text: Text,
  level: Level<WebLevel>,
  line: Line<WebLine>
) {
  let result = "";
  let style: string[] = [];
  let classes: string[] = text.Class ? text.Class : [];

  let element: Element = "span";
  let postfix = "";

  if (text.hasTag("important")) {
    element = "strong";
    classes.push("important");
  }

  if (text.hasTag("headline")) {
    element = "h1";
    classes.push("headline");
  }

  if (text.Block) {
    element = "div";
    postfix = "\n";
  }

  if (text.Code) {
    element = "code";
    postfix = "\n";
    classes.push(text.Code + "-lang");
  }

  if (text.Color) {
    style.push("color:" + text.Color);
  }

  if (text.FontWeight) {
    style.push("font-weight:" + text.FontWeight);
  }

  if (text.TextDecoration) {
    style.push("text-decoration:" + text.TextDecoration);
  }

  if (text.BorderStyle) {
    style.push("border-style:" + text.BorderStyle);
    style.push("padding:" + "5px");
    style.push("margin:" + "5px");
  }

  let indent = 0;
  if (level.info !== null && line.info !== null) {
    level.info.firstOnLine = true;
    switch (level.info.element) {
      case "ul":
      case "ol":
        element = "li";
        line.indent++;
        break;
      case "h1":
        element = "p";
        line.indent++;
        break;
      case "div":
        line.indent++;
        break;
      case "span":
      case "li":
        level.info.firstOnLine = false;
    }

    if (level.info.firstOnLine) {
      indent = line.indent * 4;
    }

    switch (text.ListStyle) {
      case "list":
        element = "ul";
        postfix = "\n";
        break;
      case "list-ordered":
        element = "ol";
        postfix = "\n";
        break;
      case "list-unordered":
        element = "ul";
        postfix = "\n";
        break;
    }

    if (line.info.nextElement) {
      element = line.info.nextElement;
      line.info.nextElement = undefined;
    }

    level.info.element = element;
  }

  if (!text.Math) {
    result +=
      " ".repeat(indent) +
      (element === "code" ? "<pre>\n" : "") +
      "<" +
      element +
      (style.length !== 0 ? ' style="' + style.join("; ") + '"' : "") +
      (classes.length !== 0 ? ' class="' + classes.join(" ") + '"' : "") +
      ">" +
      postfix;
  } else {
    result += "`\n";
  }

  return result;
}

export function webAdd(
  text: Text,
  a: string,
  b: string,
  level: Level<WebLevel>,
  line: Line<WebLine>
) {
  let result = a + b;
  return result;
}

export function webEnd(
  text: Text,
  msg: string,
  level: Level<WebLevel>,
  line: Line<WebLine>
) {
  let postfix = "";
  let prefix = "";
  let result: string = "";
  if (level.info !== null && line.info !== null) {
    switch (level.info.element) {
      case "li":
      case "p":
        postfix += "\n";
        break;
      case "div":
        prefix = "\n";
        break;
      case "code":
        prefix = "\n";
        postfix += "\n</pre>\n";
        break;
      case "h1":
        postfix += "\n";
        line.info.nextElement = "p";
        break;
    }

    if (!text.Math) {
      result = msg + prefix + "</" + level.info.element + ">";
      result =
        text.NumberFixed && text.NumberFixed > 0
          ? doFixed(result, text.NumberFixed)
          : result;
      result = text.NumberForm === "percent" ? doPercent(result) : result;

      if (text.Link !== null) {
        result = '<a href="' + text.Link + '">' + result + "</a>";
      }
      if (text.Image !== null) {
        result =
          '<img src="' +
          text.Image +
          '" alt="' +
          TextToStringSync(text) +
          '" />';
      }
    }

    result = text.Newline ? result + "<br />" : result;
  } else {
    result = msg + "\n`\n";
  }
  return result + postfix;
}
